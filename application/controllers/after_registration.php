<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    class After_registration extends CI_Controller
    {

        function __construct()
        {
            parent::__construct();
            $this->load->model('register_model', '', TRUE);
        }
        
        
        function index()
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('sex', 'Gender', 'required');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|xss_clean|is_unique[members.email]|callback_validateEmail');
            $this->form_validation->set_rules('full_name', 'Full Name', 'trim|xss_clean|');
            $this->form_validation->set_rules('birthday', 'Date of Birth', 'required');
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[6]|is_unique[members.user]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|xss_clean|min_length[6]|matches[password]');
            
            
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('register/register_view');
            } else {
                $this->check_database();
                $this->load->view('register/home_view');
                $this->output->set_header('refresh:2;url=login');
            }
        }

        function check_database()
        {
            if ($sex = $this->input->post('sex') == 1) {
                $sex = 'M';
            } else {
                $sex = 'F';
            }

            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $full_name = $this->input->post('full_name');
            $birthday = $this->input->post('birthday');
            $username = $this->input->post('username');
            $result = $this->registermodel->register($sex, $username, $password,  $email, $full_name, $birthday);
            
            if ($result) {
                $this->send($email, $full_name);
                return TRUE;
            } else {
                $this->form_validation->set_message('check_database', 'Invalid username or password');
                return false;
            }
        }

       

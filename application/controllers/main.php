<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Main extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$this->load->view('login/home_view', $data);
		}
		else
		{
			//If ther is no session the redirect to login page
			$this->load->view('pages/home');
		}
	}
	//This is the logout function which destroys the session
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		$this->session->sess_destroy();
		redirect('main', 'refresh');
	}

}
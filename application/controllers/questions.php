<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
    class Questions extends CI_Controller
    {

        public function __construct()
        {
            parent::__construct();
            $this->load->model('questions_model');
        }

        public function index()
        {
            $data['questions'] = $this->questions_model->get_questions();
            $data['title'] = 'Questions';

            $this->load->view('templates/header', $data);
            $this->load->view('questions/index', $data);
            $this->load->view('templates/footer');
        }

        public function view($slug)
        {
            $data['news_item'] = $this->news_model->get_news($slug);

            if (empty($data['news_item'])) {
                show_404();
            }

            $data['title'] = $data['questions_item']['title'];

            $this->load->view('templates/header', $data);
            $this->load->view('questions/view', $data);
            $this->load->view('templates/footer');
        }

        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Make a new question';

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('text', 'text', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('questions/create');
                $this->load->view('templates/footer');

            } else {
                $this->questions_model->set_questions();
                $this->load->view('questions/success');
            }
        }
    }
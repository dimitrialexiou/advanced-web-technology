<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
    class Questions_model extends CI_Model
    {

        public function __construct()
        {
            $this->load->database();
        }

    public function getQuest() {
		$result = $this->db->query('SELECT question_title, question_date, question_tag, question_by FROM questions');
		$data = array();
		foreach ($result->result() as $row) {
			$entry = array();
			$entry['question_title'] = $row->question_title;
			$entry['question_date'] = $row->question_date;
			$entry['question_tag'] = $row->question_tag;
			$entry['question_by'] = $row->question_by;
			$data[] = $entry;
		}
		return $data;
	}
	
	function findquestion($question)
	{
		if ($question == null || $question == '') {
			return false;
		}
	
		$this->db->where('question_title',$question);
		$res = $this->db->get('question_title');
		$data = $res->row_array();
		$res->free_result();
		return $data;
	}

    }
      
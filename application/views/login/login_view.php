<!DOCTYPE HTML>
<html>
<head>
    <title>Login</title>
  
</head>
<body>

<div id="wrapper">
    <div id="wrappertop"></div>

    <div id="wrappermiddle">

        <h2>Login</h2>

        <div id="username_input">
            <div id="username_inputleft"></div>


            <div id="username_inputmiddle">
                <?php echo validation_errors(); ?>
                <?php echo form_open('verifylogin'); ?>
                <input type="text" name="username" id="url" placeholder="Username"
                       value="<?php echo set_value('username'); ?>">
            </div>
            <div id="username_inputright"></div>
        </div>
        <div id="password_input">
            <div id="password_inputleft"></div>
            <div id="password_inputmiddle">
                <input type="password" name="password" id="url" placeholder="Password">
               
            </div>
            <div id="password_inputright"></div>
        </div>
        <div id="submit">
            <input type="image" id="submit1" name="submit"
                   value="Login">
        </div>
        </form>
        
        <div id="links_right"><?php echo anchor('register', 'Register', 'title="Register Here"'); ?></div>
    </div>
    <div id="wrapperbottom"></div>
    <h1 class="title"><a href="<?php echo base_url('index.php/main'); ?>">Back to mainpage</a></h1>


</div>

</body>
</html>

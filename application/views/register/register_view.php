<!DOCTYPE html>
<html lang="en"> 
	<head>
		<title>Registration</title>
   </head>
<body>
	<h1 class="title">Asking Music Registration Page</h1>
	<?php echo validation_errors(); ?>
	<?php $attributes = array('class' => 'register'); ?>
	<?php echo form_open('after_registration',$attributes); ?>
	
	
	<div class="rswitch">
    	<input type="radio" name="sex" value="1" id="sex_f" class="register-switch-input" checked="checked">
    	<label for="sex_m" class="register-switch-label">Male</label>
    	<input type="radio" name="sex" value="2" id="sex_m" class="register-switch-input">
    	<label for="sex_f" class="register-switch-label">Female</label>
	</div>

		<input type="text" class="input" placeholder="Username" name="username" id="username">
		<input type="password" class="input" placeholder="Password" name="password" id="password">
		<input type="password" class="input" placeholder="Repeat Password" name="password2" id="password2">
		<input type="text" class="input" placeholder="Email address" name="email" id="email" value="<?php echo set_value('email'); ?>">
		<input type="text" class="input" placeholder="Full Name" name="full_name" id="full_name" value="<?php echo set_value('username'); ?>">
		<input type="date" class="input" id="birthday" name="birthday" value="<?php echo set_value('birthday'); ?>">

		<input type="submit" value="Create Account" class="register-button" id="enter">
</form>
		<h1 class="title"><a href="<?php echo base_url('index.php/main'); ?>">Back to mainpage</a></h1>

</body>
</html>
